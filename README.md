<!--
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
SPDX-License-Identifier: Apache-2.0
-->

# Wator

Wa-Tor is a population dynamics simulation devised by Alexander Keewatin Dewdney.
This is an example implementation in Python.

## Installation

Use the package manager [poetry](https://python-poetry.org/) to install wator.

```bash
poetry install
```

## Usage

To run the program, please execute this command:

```python
poetry run ./scripts/run.py
```

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[Apache-2.0](LICENSES/Apache-2.0.txt)
