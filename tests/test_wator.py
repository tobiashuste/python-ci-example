# SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf
# <hifis-info@hzdr.de>
#
# SPDX-License-Identifier: Apache-2.0


def test_fish_creation(fish):
    """Test the object creation of Fish."""
    assert fish.size == 1
    assert fish.counter == 1

    fish.grow()

    assert fish.size == 2
    assert fish.counter == 1

    assert str(fish) == "Fish 0 | Size = 2"
