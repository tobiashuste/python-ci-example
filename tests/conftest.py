# SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf
# <hifis-info@hzdr.de>
#
# SPDX-License-Identifier: Apache-2.0

import pytest

import wator


@pytest.fixture
def fish():
    fish = wator.Fish()
    return fish
