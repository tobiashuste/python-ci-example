# SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf
# <hifis-info@hzdr.de>
#
# SPDX-License-Identifier: Apache-2.0

from random import choice, randrange

FIELD_SIZE = 50
FISH_SPAWN_THRESHOLD = 7
SHARK_SPAWN_THRESHOLD = 70
FISH_SPAWN_COST = 3
SHARK_SPAWN_COST = 30
MIN_STARTING_FISH = 20
MAX_STARTING_FISH = 100
MIN_STARTING_SHARKS = 5
MAX_STARTING_SHARKS = 30


class Fish(object):
    counter = 0

    def __init__(self):
        self.size = 1
        self.id = Fish.counter
        Fish.counter += 1

    def grow(self):
        self.size += 1

    def __repr__(self):
        return f"Fish {self.id} | Size = {self.size}"


class Shark(object):
    counter = 0

    def __init__(self):
        self.energy = 10
        self.id = Shark.counter
        Shark.counter += 1

    def hunger(self):
        self.energy -= 1 + self.energy // 20

    def eat(self, fish):
        self.energy += fish.size

    def __repr__(self):
        return f"Shark {self.id} | Energy = {self.energy}"


class Grid(object):
    def neighbors(x, y):
        return [
            (new_x, new_y)
            for new_x in [(x - 1) % FIELD_SIZE, x, (x + 1) % FIELD_SIZE]
            for new_y in [(y - 1) % FIELD_SIZE, y, (y + 1) % FIELD_SIZE]
        ]

    def free_neighbors(self, x, y):
        return [
            neighbor
            for neighbor in Grid.neighbors(x, y)
            if neighbor not in self.fishes and neighbor not in self.sharks
        ]

    def fish_neighbors(self, x, y):
        return [
            neighbor for neighbor in Grid.neighbors(x, y) if neighbor in self.fishes
        ]

    def __init__(self):
        self.fishes = {}
        self.sharks = {}

        # Place some fish
        amount = randrange(MIN_STARTING_FISH, MAX_STARTING_FISH)
        for _ in range(amount):
            x = randrange(0, FIELD_SIZE)
            y = randrange(0, FIELD_SIZE)
            self.place_fish(x, y)

        # Place some sharks
        amount = randrange(MIN_STARTING_SHARKS, MAX_STARTING_SHARKS)
        for _ in range(amount):
            x = randrange(0, FIELD_SIZE)
            y = randrange(0, FIELD_SIZE)
            self.place_shark(x, y)

    @property
    def fish_count(self):
        return len(self.fishes)

    @property
    def shark_count(self):
        return len(self.sharks)

    def place_fish(self, x, y):
        if (x, y) not in self.fishes:  # cell is still free
            self.fishes[(x, y)] = Fish()

    def place_shark(self, x, y):
        if (x, y) not in self.sharks:
            self.sharks[(x, y)] = Shark()

    def move_fish(self, old_position, new_position):
        fish = self.fishes[old_position]
        del self.fishes[old_position]
        self.fishes[new_position] = fish

    def move_shark(self, old_position, new_position):
        shark = self.sharks[old_position]
        del self.sharks[old_position]
        self.sharks[new_position] = shark

        if new_position in self.fishes:
            shark.eat(self.fishes[new_position])
            del self.fishes[new_position]

    def simulate_step(self):
        # Update fishes
        for ((x, y), fish) in self.fishes.copy().items():
            fish.grow()
            possible_moves = self.free_neighbors(x, y)
            if possible_moves:
                self.move_fish((x, y), choice(possible_moves))
                # Leave a child fish behind if possible
                if fish.size > FISH_SPAWN_THRESHOLD:
                    fish.size -= FISH_SPAWN_COST
                    self.place_fish(x, y)

        # Update sharks
        for ((x, y), shark) in self.sharks.copy().items():
            shark.hunger()
            if shark.energy < 1:
                del self.sharks[(x, y)]
                continue
            good_moves = self.fish_neighbors(x, y)
            ok_moves = self.free_neighbors(x, y)
            if good_moves:
                self.move_shark((x, y), choice(good_moves))
                if shark.energy > SHARK_SPAWN_THRESHOLD:
                    shark.energy -= SHARK_SPAWN_COST
                    self.place_shark(x, y)
            elif ok_moves:
                self.move_shark((x, y), choice(ok_moves))
                if shark.energy > SHARK_SPAWN_THRESHOLD:
                    shark.energy -= SHARK_SPAWN_COST
                    self.place_shark(x, y)

    def simulate(self, step_limit=1000):
        for current_step in range(step_limit):
            self.simulate_step()
            print(f"{current_step}, {self.fish_count}, {self.shark_count}")
            if self.shark_count < 1:
                break
