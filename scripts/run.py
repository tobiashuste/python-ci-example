#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf
# <hifis-info@hzdr.de>
#
# SPDX-License-Identifier: Apache-2.0

from wator import Grid

if __name__ == "__main__":
    planet = Grid()
    planet.simulate()
